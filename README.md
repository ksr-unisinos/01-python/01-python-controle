# 01-python-controle

Este exemplo explora a biblioteca `control`

para instalar:

```pip install control```

Para obter melhores resultados com a biblioteca de controle será utilizada também a biclioteca `scipy`

```pip install scipy```


para algumas operações será necessário a biblioteca `numpy`

```pip install numpy```

para gerar os gráficos será necessário a biblioteca `matplotlib`

```pip install matplotlib```

para gerar os videos será necessário a biblioteca `cv2`

```pip install opencv-python```


O arquivo `otim.py` executa um método de força bruta para encontrar o melhor resultado de `K` e `a` para uma malha de controle. Os resultados são gerados e armazenados em uma pasta chamada `gif` no mesmo diretório deste arquivo. Crie manualmente esta pasta.

O arquivo `video.py` converte todas as imagens na pasta `gif` em um vídeo

O arquivo `PID.py` calcula os parâmetros do PID através do método de alocação de pólos e em seguida plota o gráfico do resultado