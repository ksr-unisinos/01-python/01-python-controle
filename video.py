import os

def make_video(images, outimg=None, fps=30, size=None, is_color=True, format="XVID", outvid='image_video.avi'):
	from cv2 import VideoWriter, VideoWriter_fourcc, imread, resize
	fourcc = VideoWriter_fourcc(*format)
	vid = None
	for image in images:
		if not os.path.exists(image):
			raise FileNotFoundError(image)
		img = imread(image)
		if vid is None:
			if size is None:
				size = img.shape[1], img.shape[0]
			vid = VideoWriter(outvid, fourcc, float(fps), size, is_color)
		if size[0] != img.shape[1] and size[1] != img.shape[0]:
			img = resize(img, size)
		vid.write(img)
	vid.release()
	return vid

def geraLista():
	file = []
	p = 0
	while(True):
		p = p+1
		titulos = 'Metodo da forca Bruta'
		filename = 'gif\\'+str(p)+' '+titulos+'.png'
		if not os.path.exists(filename): return file
		file.append(filename)

files = geraLista()
#print(files)

make_video(images=files,outvid='Forca Bruta.avi')