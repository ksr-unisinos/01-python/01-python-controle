from control import TransferFunction as tf,feedback, zero
from numpy import arange, round
import matplotlib.pyplot as plt
from scipy.signal import lti, step


def Grafico(ax,ay,titulo,K,a):
	#Faz um degrau só para exibir gráficamente
	Tstep = [-ax.max()/50,0,1e-15,ax.max()]
	Ystep = [0,0,1,1]


	#prepara algumas informações da legenda
	Pico = max(ay)
	#print("Pico = ",Pico)
	Final = 1
	#print("Estabiliza em  = ",ay[-1])
	SobreSinal_c = 100*(Pico-Final)/Final
	#print("SobreSinal_c = ",SobreSinal_c,"%")
	Erro = (1-ay[-1])*100
	#print("Erro = ",Erro,"%")

	subtitulo = '\nStep Response - (K='
	subtitulo += str(round(K,3))
	subtitulo += ' a='
	subtitulo += str(round(a,3))
	subtitulo += ')'

	#Monta o Gráfico
	plt.figure()
	plt.title(subtitulo)                  # Titulo
	plt.plot(ax,ay,label='Malha Fechada: Erro = '+str(round(Erro,3))+'%' )                # Titulo
	plt.plot(Tstep,Ystep,label='Step')
	plt.plot([ax[0],ax[-1]],[Pico,Pico],label='Sobressinal: MP = '+str(round(SobreSinal_c,3))+'%' )
	plt.ylabel('gain')        # Plota o label ay
	plt.xlabel('time [s]')            # Plota o label x
	plt.grid(which='both', axis='both')     # Gride para frequências intermediárias
	plt.grid(True)                          # Mostra o Grid
	plt.margins(0, 0.1)                     # Deixa uma margem
	plt.legend()
	plt.savefig(titulo+'.png')
	plt.close('all')
	print(titulo+'.png')
	plt.clf()


#Pontos de resolução
dots = 5000
limiteDeImagens = 1000

#Parâmetros do FT experimental
wn1 = 40.21
wn2 = 207.27
k = 6417.43
TP = 0.013
MP = 0.13

#Método de Alocação de Pólos
b0 = k
a1 = wn1+wn2
a0 = wn1*wn2
g = tf([b0],[1,a1,a0])

K = arange(0.1,2,0.1)
a = arange(350,500,1)
best=[0,0,0]

def bruteForce():
	n = 0
	j = 0
	solution=[]
	maxM = 0
	titulos = 'Metodo da forca Bruta'
	
	for i in range(len(K)):
		for j in range(len(a)):
			controler = K[i]*tf([1,2*a[j],a[j]**2], [1,0]); # controlador
			sys = feedback(controler*g) # Fundação de transferência em malha fechada
			sys2 = sys.returnScipySignalLTI()[0][0]
			t,y = step(sys2,N = dots)
			m = max(y)
			if (m < (1+MP)): 
				solution.append([K[i],a[j],m])
				Grafico(t,y,'gif\\'+str(n)+' '+titulos,K[i],a[j])
				n = n+1;
				if(n == limiteDeImagens): return 
				if(maxM < m):
					maxM=m
					best=[K[i],a[j],m]

bruteForce()

K = best[0]
print(K)
a = best[1]
print(a)



